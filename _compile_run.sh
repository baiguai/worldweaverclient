javac -cp .:Release/lib/* -d ./Compiled ./Code/*.java
cd Compiled
jar -cfm ../Release/WorldWeaver.jar manifest.txt *.class
cd ../Release
java -jar WorldWeaver.jar
