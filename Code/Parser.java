import java.io.*;
import java.io.Console;
import java.lang.Object;
import java.sql.*;
import java.util.*;
import java.util.Scanner;

/*
    parser
    ----------------------------------------------------------------------------
    The parse handles player input at the highest level. - Outside of the 
    current game's scope. It handles system wide commands.
    ----------------------------------------------------------------------------
*/
public class Parser
{
    /* Properties */
    //
        private String suffix = "__in_prog";
        public String GetSuffix() { return suffix; }

        private boolean initialized = false;
        public boolean GetInitialized() { return initialized; }
        public void SetInitialized(boolean val) { initialized = val; }

        private List<String> games = Functions.ListFiles("Data/Games/", "");
        public List<String> GetGames() { return games; }
        public void SetGames(List<String> val) { games = val; }

        private Game game = null;
        public Game GetGame() { return game; }
        public void SetGame(Game val) { game = val; }
        public void ClearGame()
        {
            GetGame().DeleteGameDb();
            game = null;
        }

        private List<String> history = null;
        public List<String> GetHistory() { if (history == null) history = new ArrayList<String>(); return history; }
        public void SetHistory(List<String> val) { history = val; }
        public void AppendHistory(String val) { GetHistory().add(val); }
        public void AppendHistory(List<String> val) { GetHistory().addAll(val); }
        public void ClearHistory() { history = null; }

        public List<String> testOutput = null;
        public List<String> GetTestOutput() { if (testOutput == null) testOutput = new ArrayList<String>(); return testOutput; }
        public void SetTestOutput(List<String> val) { testOutput = val; }
        public void AddTestOutput(String val) { GetTestOutput().add(val); }
        public void ClearTestOutput() { testOutput = null; }
    //


    // Main Listener
    //  Accepts the player's input and calls the CallListener
    //  sending it the input, and checking for an escapement.
    public void Listener()
    {
        String[] args = { "" };
        Listener(args);
    }
    // Args
    //  Currently args aren't used. In the future these may be utilized to
    //  extend the client
    public void Listener(String[] args)
    {
        String input = "";
        Scanner scan = new Scanner(System.in);
        String prompt = ">> ";
        // Check the config for an initial game value
        String initGame = Functions.GetSetting("Config/Global.config", "init_game", "").trim();

        // Initialization
        //  Initialization needs to only happen once. It handles the initial
        //  game load or simply the default WW splash screen.
        if (!GetInitialized())
        {
            SetInitialized(true);

            // There is an initial game - load it.
            if (!initGame.equals(""))
            {
                // Splash Commands
                //  The splash commands come from the user's input on the
                //  branded splash screen. See UI class.
                String command = UI.Branding(initGame);

                if (command.equals("exit"))
                {
                    CallListener("exit", false);
                    return;
                }

                // Create the in-progress db - if it doesn't already exist
                try
                {
                    if (!command.equals(initGame))
                    {
                        boolean dbExists = false;

                        for (String f : GetGamesList())
                        {
                            if (f.equals(command))
                            {
                                dbExists = true;
                                break;
                            }
                        }

                        if (!dbExists) command = initGame;
                    }
                }
                catch (Exception ex) {}

                // Get the index and send it to the parser
                // to load the initial game.
                // This simulates the player choosing a game.
                int ix = 1;
                for (String f : GetGamesList())
                {
                    if (f.equals(command))
                    {
                        break;
                    }
                    else
                    {
                        ix++;
                    }
                }

                CallListener(ix + "", false);
            }
            else
            {
                // Build Welcome
                Functions.Output(UI.HomeScreen());
                ListGames();
            }
        }

        prompt =  ">> ";
        System.out.print(prompt);

        try
        {
            // Scan the player's input
            input = scan.nextLine();
            if (CallListener(input, false)) Listener();
        }
        catch (Exception ex) { ex.printStackTrace(); }
    }

    // Listener Caller
    //  Calls the parser, sending in the input and returning
    //  a boolean, allowing for an escapement.
    public boolean CallListener(String userInput, boolean testing)
    {
        boolean call = true;
        if (userInput.equals("")) return call;
        Input input = new Input(userInput);
        Response res = new Response();
        boolean newGame = false;

        // Exit
        /*
            Kills the game client, does not perform any
            state saving etc - ideally that is done as the
            player issues commands or timed events fire.
        */
        if (!res.HasResult() && Functions.StrictMatch(input.GetUserInput(), "exit"))
        {
            res.SetResult(res._success);
            call = false;
        }

        // Help
        /*
        if (!res.HasResult() &&
            GetGame().IsCurMode_Init())
        {
            if (input.CmdMatch("help"))
            {
                res.SetResult(res._success);
                res.SetOutput(UI.Help());
            }
        }
        */

        // Selecting a Game to play
        if (!res.HasResult() &&
            GetGame() == null)
        {
            try
            {
                int gameIx = Integer.parseInt(input.GetUserInput());
                String gameDb = GetGamesList().get(gameIx-1);

                // If this is a new game, make an 'in progress' copy.
                // When the game ends, delete the 'in progress' copy.
                if (gameDb.indexOf(GetSuffix()) < 0)
                {
                    newGame = true;
                    Functions.CopyFile("Data/Games/" + gameDb, "Data/Games/" + gameDb + GetSuffix());
                    gameDb = gameDb + GetSuffix();
                }

                if (!gameDb.equals(""))
                {
                    SetGame(new Game(gameDb));
                    if (newGame) res = GetGame().Parse_Game("init");
                    else res = GetGame().Parse_Game("resume");
                }
                else
                {
                    res.SetResult(res._success);
                    res.SetOutput("Be sure to select a valid game number.");
                }
            }
            catch(Exception ex) {}
        }

        // List Games
        /*
            Lists any games that are in the {root}/Data/Games
            directory. - Currently no validation is performed
            on the databases.
        */
        if (!res.HasResult() && input.CmdMatch("listgames|games"))
        {
            res.SetResult(res._success);
            ListGames();
        }

        // Up Arrow
        if (!res.HasResult() && input.CmdMatch("[A"))
        {
            // input.SetMatch(true);
            // Display command history
        }



        // Unit Test
        if (!res.HasResult() && GetGame() != null)
        {
            if (input.CmdMatch("_unittest") && GetGame().GetGame().GetDebug())
            {
                Statement cmd = GetGame().GetGlobal().GetGameCmd();
                UnitTest t = new UnitTest(cmd, input.GetParamString());
                t.SetParser(this);
                t.RunTest(cmd);
                return call;
            }
        }



        // Game
        /*
            Pass the input on to the Game object.
        */
        if (GetGame() != null && !res.HasResult())
        {
            res = GetGame().Parse_Game(input.GetUserInput());
            if (res.GetResult().equals(res._success))
            {
                input.SetMatch(true);
            }
        }

        // Test
        if (!res.HasResult() && GetGame() != null)
        {
            if (input.CmdMatch("_test") && GetGame().GetGame().GetDebug())
            {
                boolean isRec = GetGame().GetRecording();
                Statement cmd = GetGame().GetGlobal().GetGameCmd();
                List<String> cmds = new ArrayList<String>();
                cmds = Parse_Test(cmd, input.GetParamString());

                for (String s : cmds)
                {
                    if (isRec) GetGame().SetRecording(false);
                    String[] cmdArr = s.split("\\n");
                    for (String c : cmdArr)
                    {
                        call = CallListener(c, false);
                    }
                    if (isRec) GetGame().SetRecording(true);
                }

                return call;
            }
            else
            {
                if (!GetGame().GetGame().GetIsPlaying())
                {
                    Functions.Output(UI.HomeScreen());
                    ListGames();
                }
            }
        }



        // FINAL OUTPUT //

        int wrap = 80;
        String noOut = "";
        // Get the wrap_width setting from the Game
        if (GetGame() != null)
        {
            Attribute attribute = new Attribute();
            Element attr = attribute.GetAttribute(GetGame().GetGlobal().GetGameCmd(), GetGame().GetGame(), "wrap_width");
            if (attr != null)
            {
                try
                {
                    wrap = Integer.parseInt(attr.GetValue());
                }
                catch (Exception ex) {}
            }
        }

        // There is output - send it to the console.
        if (res.HasResult() && res.GetOutputSize() > 0)
        {
            Global_Data global = new Global_Data();

            // If there is a display_mode attribute 'clear', clear the console
            if (!testing)
            {
                if (GetGame().GetGame().GetAttribute("display_mode") != null &&
                    GetGame().GetGame().GetAttribute("display_mode").GetValue().equals("clear"))
                {
                    Functions.ClearConsole();
                }
            }

            noOut = NoOutputLogic(res, userInput);

            if (!testing)
            {
                if (!noOut.equals("")) res.AppendOutput(noOut);
                Functions.OutputWrapped(res.GetOutput(), wrap);
            }
            else
            {
                SetTestOutput(res.GetOutput());
                AddTestOutput("");
                res.ClearOutput();
            }
        }
        if (!res.HasResult())
        {
            noOut = NoOutputLogic(res, userInput);
            if (!noOut.equals("")) res.AppendOutput(noOut);
            Functions.OutputWrapped(res.GetOutput(), wrap);
        }

        // The game was started but has ended
        if (GetGame() != null && !GetGame().GetGame().GetIsPlaying())
        {
            ClearGame();
        }

        return call;
    }


    private String NoOutputLogic(Response res, String userInput)
    {
        String output = "";
        boolean outputFound = true;
        boolean handled = false;
        String test = "";

        if (!handled && res.GetOutput().size() < 1)
        {
            handled = true;
            outputFound = false;
        }

        if (!handled)
        {
            test = "";

            for (String s : res.GetOutput())
            {
                test += s;
            }

            if (test.trim().equals(""))
            {
                handled = true;
                outputFound = false;
            }
        }

        if (!outputFound)
        {
            handled = false;

            if (!handled && Functions.RegMatch("examine*|x*", userInput))
            {
                handled = true;
                output = "I'm not sure what you are trying to examine.";
            }

            if (!handled && Functions.RegMatch("take *|pick *up* ", userInput))
            {
                handled = true;
                output = "I'm not sure what you are trying to take.";
            }

            if (!handled && Functions.RegMatch("drop *|put * down*", userInput))
            {
                handled = true;
                output = "I'm not sure what you are trying to drop.";
            }

            if (!handled && Functions.RegMatch("use *", userInput))
            {
                handled = true;
                output = "I'm not sure what you are trying to use.";
            }

            if (!handled)
            {
                handled = true;
                output = "I didn't understand that.";
            }
        }

        return output;
    }


    // List Games
    // TODO: Validate the game DBs and alert player of any issues
    private void ListGames()
    {
        int ix = 1;

        if (GetGames().size() < 1)
        {
            Functions.OutputRaw("\n\n");
            Functions.OutputRaw("To make games available for play simply copy the game databases into\n<root>/Data/Games");
            Functions.OutputRaw("\n\n");
            return;
        }

        Functions.OutputRaw("\n\n");
        Functions.OutputRaw("Available Games (Enter a number to select a game)\n");
        Functions.OutputRaw("New Games:\n");
        for (String f : GetGames())
        {
            if (f.indexOf(GetSuffix()) < 0)
            {
                Functions.OutputRaw(ix + " - " + f);
                ix++;
            }
        }
        Functions.OutputRaw("\n\nResume Games:\n");
        for (String f : GetGames())
        {
            if (f.indexOf(GetSuffix()) >= 0)
            {
                Functions.OutputRaw(ix + " - " + f);
                ix++;
            }
        }
        Functions.OutputRaw("\n\n");
    }
    private List<String> GetGamesList()
    {
        List<String> output = new ArrayList<String>();

        if (GetGames().size() < 1)
        {
            return output;
        }

        for (String f : GetGames())
        {
            if (f.indexOf(GetSuffix()) < 0)
            {
                output.add(f);
            }
        }
        for (String f : GetGames())
        {
            if (f.indexOf(GetSuffix()) >= 0)
            {
                output.add(f);
            }
        }

        return output;
    }


    // Parse Test
    private List<String> Parse_Test(Statement cmd, String testName)
    {
        List<String> output = new ArrayList<String>();
        String sql = "";

        testName = Functions.Encode(testName);

        try
        {
            ResultSet rs;

            sql =  "";
            sql += "SELECT ";
            sql += "    Commands ";
            sql += "FROM ";
            sql += "    Test ";
            sql += "WHERE 1 = 1 ";
            sql += "    AND TestName = '" + testName + "' ";
            sql += ";";

            rs = cmd.executeQuery(sql);

            while (rs.next())
            {
                output.add(rs.getString(1));
            }

            rs.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            Functions.Error(sql);
        }

        return output;
    }
}
