import java.io.*;
import java.io.Console;
import java.sql.*;
import java.util.*;
import java.util.Random;
import java.util.Scanner;

/*
    FIGHT HELPER METHODS
    ----------------------------------------------------------------------------
    ----------------------------------------------------------------------------
*/

public class Fight
{
    /* PROPERTIES */
    //
        private boolean initialAttack = true;
        private int PlayerLife = -1;
        private int OppLife = -1;

        private List<String> output = null;
        public List<String> GetOutput() { if (output == null) output = new ArrayList<String>(); return output; }
        public void SetOutput(List<String> val) { output = val; }
        public void AddOutput(String val) { GetOutput().add(val); }
        public boolean HasOutput() { return GetOutput().size() > 0; }
        public void ClearOutput() { output = null; }
    //

    public List<String> Parse_Fight(Statement cmd, Element parent)
    {
        initialAttack = true;
        Element opponent = parent.GetNpc(parent);
        Element player = parent.GetPlayer(parent);

        if (opponent == null || player == null) GetOutput();

        Global_Data global = new Global_Data();
        // If there is a display_mode attribute 'clear', clear the console
        if (global.GetAttributeValue(cmd, "{game}:display_mode").equals("clear"))
        {
            Functions.ClearConsole();
        }

        // Add messages to the output
        Listener(cmd, opponent, player);

        return GetOutput();
    }


    // Fight input loop
    public void Listener(Statement cmd, Element opponent, Element player)
    {
        String input = "";
        Attribute attribute = new Attribute();
        Event event = new Event();
        Scanner scan = new Scanner(System.in);
        String prompt = "";
        Element game = player.GetGame(player);

        attribute.LoadAttributes(cmd, opponent);
        attribute.LoadAttributes(cmd, player);
        event.LoadEvents(cmd, opponent);

        try
        {
            if (PlayerLife < 0) PlayerLife = Integer.parseInt(player.GetAttributeValue("life"));
            if (OppLife < 0) OppLife = Integer.parseInt(opponent.GetAttributeValue("life"));
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }

        if (PlayerLife < 1 || OppLife < 1) return;

        if (initialAttack)
        {
            List<String> tmp = new ArrayList<String>();
            tmp.addAll(GetFightMessage(cmd, player, opponent, false, "attack"));
            Functions.Output(tmp);
            // Functions.Output("You are being attacked.");
            initialAttack = false;
        }

        if (PlayerLife > 0 && OppLife > 0)
        {
            if (HasOutput())
            {
                Functions.Output(GetOutput());
                ClearOutput();
            }

            Functions.Output("1 - attack or 2 - flee\n\n");

            System.out.print("pl: " + PlayerLife + ", en: " + OppLife + ">> ");

            try
            {
                input = scan.nextLine();
                if (CallListener(cmd, input, opponent, player)) Listener(cmd, opponent, player);
            }
            catch (Exception ex) { ex.printStackTrace(); }
        }
    }
    public boolean CallListener(Statement cmd, String userInput, Element opponent, Element player)
    {
        Input input = new Input(userInput);
        Element game = player.GetGame(player);
        Object object = new Object();
        Element playerWeapon = object.GetArmedWeapon(cmd, player);
        Response res = new Response();
        boolean call = true;

        if (!input.GetMatch() && Functions.Match(input.GetInputCommand(), "quit|exit"))
        {
            call = false;
            return call;
        }

        if (game.GetDebug())
        {
            if (!input.GetMatch() && Functions.Match(input.GetInputCommand(), "_kill"))
            {
                input.SetMatch(true);
                Attribute attribute = new Attribute();
                Element att = attribute.GetAttribute(cmd, opponent, "life");
                OppLife = 0;
                att.SetValue("0");
                attribute.SaveAttribute(cmd, att);
                input.AppendOutput(UI.Kill());
                call = false;
            }

            if (!input.GetMatch() && Functions.Match(input.GetInputCommand(), "_die"))
            {
                input.SetMatch(true);
                Attribute attribute = new Attribute();
                Element att = attribute.GetAttribute(cmd, player, "life");
                att.SetValue("0");
                PlayerLife = 0;
                attribute.SaveAttribute(cmd, att);
                input.AppendOutput(UI.GameOver());
                call = false;
            }

            if (!input.GetMatch() && Functions.Match(input.GetInputCommand(), "_enemy"))
            {
                input.AppendOutput(GetEnemyDetails(cmd, opponent));
            }
        }

        if (!input.GetMatch() && Functions.Match(input.GetInputCommand(), "attack|1"))
        {
            input.SetMatch(true);
            input.AppendOutput(DoAttack(cmd, opponent, player));

            // Do die check
            if (PlayerLife == 0 || OppLife == 0)
            {
                call = false;
            }
        }

        if (!input.GetMatch() && Functions.Match(input.GetInputCommand(), "flee|2"))
        {
            input.SetMatch(true);
            input.AppendOutput(DoAttack(cmd, opponent, player, true));

            // Do die check
            if (DoDieCheck())
            {
                call = false;
                game.SetIsPlaying(false);
                input.SetOutput(UI.GameOver());
            }

            // Do kill check
            if (DoKillCheck())
            {
                call = false;
                input.SetOutput(GetFightMessage(cmd, playerWeapon, opponent, true, "kill"));
            }

            call = false;
        }

        if (input.HasOutput())
        {
            SetOutput(input.GetOutput());
        }

        return call;
    }



    private List<String> DoAttack(Statement cmd, Element opponent, Element player)
    {
        return DoAttack(cmd, opponent, player, false);
    }

    private List<String> DoAttack(Statement cmd, Element opponent, Element player, boolean enemyAttacking)
    {
        List<String> output = new ArrayList<String>();
        Element game = player.GetGame(player);
        Object object = new Object();
        Attribute attribute = new Attribute();
        Element playerWeapon = object.GetArmedWeapon(cmd, player);
        Element opponentWeapon = object.GetArmedWeapon(cmd, opponent);
        int playerInit = 80; // Initiative change defaults to 80/100
        int opponentInit = 20; // Initiative change defaults to 20/100
        int plInitCheck = 0;
        int opInitCheck = 0;
        String tmp = "";

        tmp = attribute.GetAttributeValue(cmd, player, "initiative");
        if (!tmp.equals(""))
        {
            try
            {
                playerInit = Integer.parseInt(tmp);
            }
            catch (Exception ex)
            {
                playerInit = 80;
            }
        }
        tmp = attribute.GetAttributeValue(cmd, opponent, "initiative");
        if (!tmp.equals(""))
        {
            try
            {
                opponentInit = Integer.parseInt(tmp);
            }
            catch (Exception ex)
            {
                opponentInit = 20;
            }
        }

        if (playerWeapon == null || opponentWeapon == null) return output;

        if (!enemyAttacking)
        {
            // Get the initiative
            tmp = attribute.GetAttributeValue(cmd, opponentWeapon, "speed");
            if (!tmp.equals(""))
            {
                try
                {
                    opponentInit = Integer.parseInt(tmp);
                }
                catch (Exception ex)
                {
                    opponentInit = 20;
                }
            }
            tmp = attribute.GetAttributeValue(cmd, playerWeapon, "speed");
            if (!tmp.equals(""))
            {
                try
                {
                    playerInit = Integer.parseInt(tmp);
                }
                catch (Exception ex)
                {
                    playerInit = 80;
                }
            }

            plInitCheck = Functions.RandomInt(playerInit, 100);
            opInitCheck = Functions.RandomInt(opponentInit, 100);
        }
        else
        {
            plInitCheck = 0;
            opInitCheck = 100;
        }

        if (plInitCheck > opInitCheck)
        {
            // Player Attacks
            output.addAll(Functions.CleanList(DoPlayerAttack(cmd, opponent, opponentWeapon, player, playerWeapon)));
        }
        else
        {
            // Opponent Attacks
            output.addAll(Functions.CleanList(DoOpponentAttack(cmd, opponent, opponentWeapon, player, playerWeapon)));
        }

        if (DoKillCheck())
        {
            output.addAll(GetFightMessage(cmd, playerWeapon, opponent, true, "kill"));
        }

        if (DoDieCheck())
        {
            game.SetIsPlaying(false);
            output.add(UI.GameOver());
        }

        return output;
    }

    private boolean DoKillCheck()
    {
        boolean output = false;

        if (OppLife < 0) OppLife = 0;

        if (OppLife < 1) output = true;

        return output;
    }
    private boolean DoDieCheck()
    {
        boolean output = false;

        if (PlayerLife < 0) PlayerLife = 0;

        if (PlayerLife < 1) output = true;

        return output;
    }

    private List<String> DoPlayerAttack(Statement cmd, Element opponent, Element opponentWeapon, Element player, Element playerWeapon)
    {
        List<String> output = new ArrayList<String>();
        Attribute attribute = new Attribute();
        Element game = player.GetGame(player);
        int accuracy = 0; // Default accuracy to 50/50
        int minCheck = 50; // Default min accuracy check to 50/50
        int damage = 2; // Default damage to 2
        int oppLife = 0; // Default the opponent's life to 0
        String tmp = "";

        tmp = attribute.GetAttributeValue(cmd, playerWeapon, "accuracy");
        if (!tmp.equals(""))
        {
            try
            {
                accuracy = Integer.parseInt(tmp);
            }
            catch (Exception ex)
            {
                accuracy = 0;
            }
        }

        if (Functions.RandomInt(accuracy, 100) > minCheck)
        {
            // Do the attack
            tmp = attribute.GetAttributeValue(cmd, playerWeapon, "damage");
            if (!tmp.equals(""))
            {
                try
                {
                    damage = Integer.parseInt(tmp);
                }
                catch (Exception ex)
                {
                    damage = 2;
                }
            }

            tmp = attribute.GetAttributeValue(cmd, opponent, "life");
            if (!tmp.equals(""))
            {
                try
                {
                    oppLife = Integer.parseInt(tmp);
                }
                catch (Exception ex)
                {
                    oppLife = 0;
                }
            }

            // Do the damage
            oppLife = oppLife -= damage;

            if (oppLife < 0) oppLife = 0;

            Element att = attribute.GetAttribute(cmd, opponent, "life");
            att.SetValue(oppLife + "");
            attribute.SaveAttribute(cmd, att);
            opponent.SetAttribute("life", oppLife + "");
            OppLife = oppLife;

            if (oppLife > 0)
            {
                output.addAll(GetFightMessage(cmd, playerWeapon, opponent, true, "attack"));
            }
            else
            {
                return output;
            }
        }
        else
        {
            output.addAll(GetFightMessage(cmd, playerWeapon, opponent, true, "miss"));
        }

        return output;
    }

    private List<String> DoOpponentAttack(Statement cmd, Element opponent, Element opponentWeapon, Element player, Element playerWeapon)
    {
        List<String> output = new ArrayList<String>();
        Attribute attribute = new Attribute();
        Element game = player.GetGame(player);
        int accuracy = 0; // Default accuracy to 50/50
        int minCheck = 50; // Default min accuracy check to 50/50
        int damage = 2; // Default damage to 2
        int playerLife = 0; // Default the player's life to 0
        String tmp = "";

        tmp = attribute.GetAttributeValue(cmd, opponentWeapon, "accuracy");
        if (!tmp.equals(""))
        {
            try
            {
                accuracy = Integer.parseInt(tmp);
            }
            catch (Exception ex)
            {
                accuracy = 0;
            }
        }

        if (Functions.RandomInt(accuracy, 100) > minCheck)
        {
            // Do the attack
            tmp = attribute.GetAttributeValue(cmd, opponentWeapon, "damage");
            if (!tmp.equals(""))
            {
                try
                {
                    damage = Integer.parseInt(tmp);
                }
                catch (Exception ex)
                {
                    damage = 2;
                }
            }

            tmp = attribute.GetAttributeValue(cmd, player, "life");
            if (!tmp.equals(""))
            {
                try
                {
                    playerLife = Integer.parseInt(tmp);
                }
                catch (Exception ex)
                {
                    playerLife = 0;
                }
            }

            // Do the damage
            playerLife = playerLife -= damage;

            if (playerLife < 0) playerLife = 0;

            Element att = attribute.GetAttribute(cmd, player, "life");
            att.SetValue(playerLife + "");
            attribute.SaveAttribute(cmd, att);
            player.SetAttribute("life", playerLife + "");
            PlayerLife = playerLife;

            if (playerLife > 0)
            {
                output.addAll(GetFightMessage(cmd, playerWeapon, opponent, false, "attack"));
            }
            else
            {
                output.add("You have been killed.");
            }
        }
        else
        {
            output.addAll(GetFightMessage(cmd, playerWeapon, opponent, false, "miss"));
        }

        return output;
    }

    public List<String> GetEnemyDetails(Statement cmd, Element enemy)
    {
        List<String> output = new ArrayList<String>();
        List<String> tmp = new ArrayList<String>();
        Attribute attribute = new Attribute();
        Object object = new Object();
        Event event = new Event();
        Command command = new Command();

        output.add("Enemy " + enemy.GetLabel() + ":");
        output.add("- Alias: " + enemy.GetGuid());
        output.add("- Meta: " + enemy.GetMeta());

        attribute.LoadAttributes(cmd, enemy);
        if (enemy.HasAttributes())
        {
            output.add("- Attributes:");
            for (Element attr : enemy.GetAttributes())
            {
                output.add("   - " + attr.GetGuid() + ", " + attr.GetValue());
            }
        }

        event.LoadEvents(cmd, enemy);
        if (enemy.HasEvents())
        {
            tmp = new ArrayList<String>();
            for (Element evt : enemy.GetEvents())
            {
                if (!tmp.contains("   - Type: " + evt.GetType()))
                {
                    tmp.add("   - Type: " + evt.GetType());
                }
            }
            if (tmp.size() > 0)
            {
                output.add("- Events:");
                for (String s : tmp)
                {
                    output.add(s);
                }
            }
        }

        object.LoadObjects(cmd, enemy);
        if (enemy.HasObjects())
        {
            output.add("- Objects: ");
            for (Element obj : enemy.GetObjects())
            {
                output.add("   - " + obj.GetLabel() + " (" + obj.GetGuid() + ")");
            }
        }

        command.LoadCommands(cmd, enemy);
        if (enemy.HasCommands())
        {
            tmp = new ArrayList<String>();
            for (Element comm : enemy.GetCommands())
            {
                if (!tmp.contains("   - Syntax: " + comm.GetSyntax()))
                {
                    tmp.add("   - Syntax: " + comm.GetSyntax());
                }
            }
            if (tmp.size() > 0)
            {
                output.add("- Commands:");
                for (String s : tmp)
                {
                    output.add(s);
                }
            }
        }

        output.add("");

        return output;
    }


    /* FIRE FIGHT EVENTS, GRABBING THEIR OUTPUT */
    public List<String> GetFightMessage(Statement cmd, Element playerWeapon, Element opponent, boolean playerAttacking, String eventType)
    {
        List<String> output = new ArrayList<String>();
        Event event = new Event();
        String msg = "";

        switch (eventType)
        {
            case "attack":
                if (playerAttacking)
                {
                    msg = "You have attacked enemy:fight_label.";
                }
                else
                {
                    msg = "enemy:fight_proper_label has attacked you.";
                }
                break;

            case "miss":
                if (playerAttacking)
                {
                    msg = "Your attack on enemy:fight_label has failed.";
                }
                else
                {
                    msg = "enemy:fight_proper_label's attack on you has failed.";
                }
                break;

            case "kill":
                if (playerAttacking)
                {
                    msg = "You have enemy:fight_kill enemy:fight_label.";
                }
                else
                {
                    msg = "You have been killed.";
                }
                break;
        }

        if (playerAttacking)
        {
            event.LoadEvents(cmd, playerWeapon);
            output = event.Parse_Events(cmd, playerWeapon, eventType, false);
            if (output.size() < 1)
            {
                output.add(msg);
            }
        }
        else
        {
            event.LoadEvents(cmd, opponent);
            output = event.Parse_Events(cmd, opponent, eventType, false);
            if (output.size() < 1)
            {
                output.add(msg);
            }
        }

        output = FixFightMessage(cmd, output, opponent);

        return output;
    }

    public List<String> FixFightMessage(Statement cmd, List<String> input, Element opponent)
    {
        List<String> output = new ArrayList<String>();
        Attribute attr = new Attribute();
        attr.LoadAttributes(cmd, opponent);

        for (String s : input)
        {
            s = s.replace("enemy:fight_label", attr.GetAttributeValue(cmd, opponent, "fight_label", "the opponent"));
            s = s.replace("enemy:fight_proper_label", attr.GetAttributeValue(cmd, opponent, "fight_proper_label", "The opponent"));
            s = s.replace("enemy:fight_kill", attr.GetAttributeValue(cmd, opponent, "fight_kill", "killed"));
            output.add(s);
        }

        return output;
    }
}
